(function () {
  // Get a data set. 10 points.
  function _getData () {
    return d3.range(24).map(function(i) {return _getDataPoint(i)})
  }

  // Get a single point of data.
  function _getDataPoint(i) {
    var y_value = null;
    /*$.ajax({
      url: 'get_weather.php?temperature',
      type: 'get',
      dataType: 'html',
      async: false,
      success: function(data) {
        y_value = data
      }
    });*/
    //return {x: i/10, y: y_value}
    return {x: i, y: Math.random(8) + 12}
  }

  // Slide a point to the left.
  function _slideDataPoint(datum, i) {
    return {x: (i-1), y:datum.y}
  }

  // Add a data point to the right of the graph and slide the line to the
  // left.
  function addData() {
    // Grab the path
    var path = d3.select("path")
    // Grab the data from the path
    var data = path[0][0].__data__

    // Slap a new random point to the end of the data
    data.push(_getDataPoint(data.length))
    // Get rid of the first point
    data.shift()

    // Adjust the X value for each point
    for (i = 0; i < data.length; i++) {
      data[i] = _slideDataPoint(data[i], i)
    }

    // Apply the new data to the path and re-draw. 
    path
      .data([data])
      .transition()
        .duration(1000)
        // Use a linear easing to keep an even scroll
        .ease("linear")
        .attr("d", d3.svg.line()
          .x(function(d) {return x(d.x)})
          .y(function(d) {return y(d.y)})
          // I'm not sure if this is the interpolation that works best, but I
          // can't find a better one...
          //.interpolate("basis")
        )   
  }

  // Set up the parameters of the chart object.
  var width = 550,
      height = 275,
      x = d3.scale.linear().domain([0, 24]).range([0, width]),
      y = d3.scale.linear().domain([10, 20]).range([height, 0]),
      xAxis = d3.svg.axis().scale(x).tickSize(-height).tickSubdivide(true),
      //xAxis = d3.svg.axis().scale(x).ticks(10),
      yAxis = d3.svg.axis().scale(y).ticks(4).orient("right");

  // Create the chart in its initial state
  var chart = d3.select("body")
                .data([_getData()])
                .append("svg:svg")
                  .attr("width",  width)
                  .attr("height", height);

  chart.append("svg:clipPath")
    .attr("id", "clip")
    .append("svg:rect")
      .attr("width", width)
      .attr("height", height);

  chart.append("svg:path")
      .attr("class", "line")
      .attr("clip-path", "url(#clip)")
      .attr("d", d3.svg.line()
        .x(function(d) { return x(d.x); })
        .y(function(d) { return y(d.y); })
        //.interpolate("basis")
      );

  chart.append("svg:g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,275)")
    .call(xAxis);

  chart.append("svg:g")
    .attr("class", "y axis")
    .attr("transform", "translate(550, 0)")
    .call(yAxis);

  /*chart.append("svg:line")
    .attr("x1", 0)
    .attr("x2", width * 9)
    .attr("y1", height)
    .attr("y2", height)
    .attr("stroke", "#000");

  chart.append("svg:line")
    .attr("x1", 0)
    .attr("x2", 0)
    .attr("y1", 0)
    .attr("y2", height)
    .attr("stroke", "#000");*/

 /* var ticks = chart.selectAll('.tick')
    .data(y.ticks(5))
    .enter().append('svg:g')
    .attr('transform', "translate(0, " + function(d){return y(d);} + ")")
    .attr('class', 'tick');

  ticks.append('svg:line')
    .attr('y1', 0)
    .attr('y2', height)
    .attr('x1', 0)
    .attr('x2', 0);*/

  chart.append('svg:text')
    .text(function(d){ return d; })
    .attr('text-anchor', 'end')
    .attr('dy', 2)
    .attr('dx', -4);


  // Add a new data point every second.
  window.setInterval(addData, 1000)

})()
