var cursor = 0;
var all_data = [];

$.ajax(
{
    url: 'LoggedData.json',
    dataType: 'json',
    async: false,
    success: function(jdata) {
      $.each(jdata, function(key, val) {
        all_data.push({ctime: key, time: key, value: parseFloat(val.temp), humid: parseFloat(val.humid)});
      });
    }
});

var w = 900;
var h = 300;
var max = 0;
var min = 100;
var hmax = 0;
var hmin = 1000;


//console.log(data.length);
for (var i = 0; i < all_data.length; i++) {
  //console.log(data[i]);
  if (max < all_data[i].value) {
    max = all_data[i].value;
  }
  if (min > all_data[i].value) {
    min = all_data[i].value;
  }
  if (hmax < all_data[i].humid) {
    hmax = all_data[i].humid;
  }
  if (hmin > all_data[i].humid) {
    hmin = all_data[i].humid;
  }
}

var date = new Date();
var time = date.getTime()/1000;
var maxTime = 0;
var minTime= 1000;
var domain = [];

for (var i = 0; i < all_data.length; i++) {
  time_in_minutes = (time - all_data[i].time)/60
  if (maxTime < time_in_minutes) {
    //max = time_in_minutes;
  }
  if (minTime > time_in_minutes) {
   // min = time_in_minutes;
  }
  all_data[i].time = parseInt(time_in_minutes);
  domain.push(parseInt(time_in_minutes));
}

domain.sort(function(a,b){return a - b;});

var data = [];
var testdata = [];
var XLENGTH = 60*12;

function get_start_ten(domain, all_data) {
  data = [];
  testdata = [];
  for (var i = 0; i < XLENGTH; i++) {
    for (j in all_data) {
      if (domain[i] == all_data[j].time) {
        data.push(all_data[j]);
        testdata.push(domain[i]);
        break;
      }
    }
  }
  display_data(testdata, data);
  display_humid_data(testdata, data);
}

function get_ten_new(domain, all_data) {
  data = [];
  testdata = [];
  for (var i = cursor - XLENGTH; i < cursor; i++) {
    for (j in all_data) {
      if (domain[i] == all_data[j].time) {
        data.push(all_data[j]);
        testdata.push(domain[i]);
        break;
      }
    }
  }
  cursor = cursor - XLENGTH;
  display_data(testdata, data);
  display_humid_data(testdata, data);
} 

function get_ten_old(domain, data) {
  data = [];
  testdata = [];
  for (var i = cursor + XLENGTH - 1; i < cursor + XLENGTH + XLENGTH; i++) {
    for (j in all_data) {
      if (domain[i] == all_data[j].time) {
        data.push(all_data[j]);
        testdata.push(domain[i]);
        break;
      }
    }
  } 
  cursor = cursor + XLENGTH;
  display_data(testdata, data);
  display_humid_data(testdata, data);
}

$("#back").click(function() {
  get_ten_old(domain, all_data);
});

$("#forward").click(function() {
  get_ten_new(domain, all_data);
});

var minDomain = d3.min(domain);
var maxDomain = d3.max(domain);

function display_data(testdata, data) {
  var minx = d3.min(testdata);
  var maxx = d3.max(testdata);

  var x = d3.scale.linear().domain([maxx, minx]).range([80, w-40]);
  var y = d3.scale.linear().domain([Math.floor(min), Math.ceil(max)]).range([h-20, 20]);
  var XOFFSET = 80;
  var YOFFSET = 80;

  //Create the base visual layer
  d3.select('#chart').select('svg').remove();

  var visual = d3.select('#chart')
                .append('svg:svg')
                  .attr('width', w)
                  .attr('height', h + YOFFSET);

  var yTicks = visual.selectAll(".y-tick")
    .data(y.ticks(7))
    .enter().append("svg:g")
      .attr("transform", function(d) { 
        return "translate(" + XOFFSET + ", " + y(d)  + ")"; 
      })
      .attr("class", "tick");

  yTicks.append("svg:line")
    .attr("y1", 0)
    .attr("y2", 0)
    .attr("x1", 0)
    .attr("x2", w);

  yTicks.append('svg:text')
    .text(function(d) { return d; })
    .attr('text-anchor', 'end')
    .attr('dy', 2)
    .attr('dx', -4);

  var xTicks = visual.selectAll(".x-tick")
    .data(x.ticks(10))
    .enter().append("svg:g")
      .attr("transform", function(d, i) { 
        console.log("(" + i + ", " + d + ")");
        return "translate( " + x(d) + ", 0)"; 
      })
      .attr("class", "tick");

  xTicks.append("svg:line")
    .attr("y1", 0)
    .attr("y2", h)
    .attr("x1", 0)
    .attr("x2", 0);

  xTicks.append('svg:text')
    .text(function(d) { 
          var date = new Date();
          var time_in_mill = date.getTime() - d*60*1000;
          var new_data = new Date(time_in_mill);
          var minutes = new_data.getMinutes();
          var sminutes = minutes < 10 ? "0" + minutes : minutes;
          var hours = new_data.getHours();
          var shours = hours < 10 ? "0" + hours : hours;

          return shours + ":" + sminutes; 
      })
    .attr('text-anchor', 'end')
    .attr('y', h)
    .attr('dy', 15)
    .attr('dx', 25);

  var xTitle = visual.append("svg:g")
      .attr("text-anchor", "end")
      .attr("transform", "translate(550," + (h+50) + ")");

  xTitle.append("svg:text")
    .attr("class", "xTitle")
    .text("Time in Hours");

  var yTitle = visual.append("svg:g")
      .attr("text-anchor", "end")
      .attr("transform", "translate(100," + (h-400) + ")");

  yTitle.append("svg:text")
    .attr("class", "yTitle")
    .attr("transform", "rotate(-90 -75, 0), translate(-200,0)")
    .text("Degrees in Celsius");

  visual.selectAll('path.line')
    .data([data])
    .enter().append("svg:path")
      .attr("color", "#000")
      .attr("stroke", "steelblue")
      .attr("d", d3.svg.line()
          .x(function(d,i){ return x(d.time); })
          .y(function(d){ return y(d.value); }));

}

function display_humid_data(testdata, data) {
  var minx = d3.min(testdata);
  var maxx = d3.max(testdata);

  var x = d3.scale.linear().domain([maxx, minx]).range([80, w-40]);
  var y = d3.scale.linear().domain([Math.floor(hmin) - 1, Math.ceil(hmax)]).range([h-20, 20]);
  var XOFFSET = 80;
  var YOFFSET = 80;

  //Create the base visual layer
  d3.select('#chart2').select('svg').remove();

  var visual = d3.select('#chart2')
                .append('svg:svg')
                  .attr('width', w)
                  .attr('height', h + YOFFSET);

  var yTicks = visual.selectAll(".y-tick")
    .data(y.ticks(7))
    .enter().append("svg:g")
      .attr("transform", function(d) { 
        return "translate(" + XOFFSET + ", " + y(d)  + ")"; 
      })
      .attr("class", "tick");

  yTicks.append("svg:line")
    .attr("y1", 0)
    .attr("y2", 0)
    .attr("x1", 0)
    .attr("x2", w);

  yTicks.append('svg:text')
    .text(function(d) { return d; })
    .attr('text-anchor', 'end')
    .attr('dy', 2)
    .attr('dx', -4);

  var xTicks = visual.selectAll(".x-tick")
    .data(x.ticks(10))
    .enter().append("svg:g")
      .attr("transform", function(d, i) { 
        console.log("(" + i + ", " + d + ")");
        return "translate( " + x(d) + ", 0)"; 
      })
      .attr("class", "tick");

  xTicks.append("svg:line")
    .attr("y1", 0)
    .attr("y2", h)
    .attr("x1", 0)
    .attr("x2", 0);

  xTicks.append('svg:text')
    .text(function(d) { 
          var date = new Date();
          var time_in_mill = date.getTime() - d*60*1000;
          var new_data = new Date(time_in_mill);
          var minutes = new_data.getMinutes();
          var sminutes = minutes < 10 ? "0" + minutes : minutes;
          var hours = new_data.getHours();
          var shours = hours < 10 ? "0" + hours : hours;

          return shours + ":" + sminutes; 
    })
    .attr('text-anchor', 'end')
    .attr('y', h)
    .attr('dy', 15)
    .attr('dx', 25);

  var xTitle = visual.append("svg:g")
      .attr("text-anchor", "end")
      .attr("transform", "translate(550," + (h+50) + ")");

  xTitle.append("svg:text")
    .attr("class", "xTitle")
    .text("Time in Hours");

  var yTitle = visual.append("svg:g")
      .attr("text-anchor", "end")
      .attr("transform", "translate(100," + (h-400) + ")");

  yTitle.append("svg:text")
    .attr("class", "yTitle")
    .attr("transform", "rotate(-90 -75, 0), translate(-200,10)")
    .text("Relative Humidity %");

  visual.selectAll('path.line')
    .data([data])
    .enter().append("svg:path")
      .attr("stroke", "green")
      .attr("color", "#000")
      .attr("d", d3.svg.line()
          .x(function(d,i){ return x(d.time); })
          .y(function(d){ return y(d.humid); }));

}


get_start_ten(domain, all_data);

